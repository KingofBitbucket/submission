# Memory-constrained Device Placement Algorithms
The algorithms can be run on any NN models. We'll be using Inception-V3 as an example.

## File Hierarachy
The algorithms lie in the Simulator folder. The Inception-V3 folder is downloaded from the TensorFlow Github Repository. The inception_train.py has been modified to distributed version and called the selected algorithm at line 362. Feel free to change the algorithm.

    Simulator
        Simulate.py
        m_sct.py
        m_etf.py
        m_topo.py
    Inception-V3
        g3doc
        inception
        README.md
        WORKSPACE
    inception_train.py
    timeline.json
    timeline_memory.json
    mosek.lic

## Installation
Note: The following installation should be applied to all the devices.
* Basic Dependencies

      pip install tensorflow
      pip install networkx
      pip install matplotlib
      pip install scipy

* Mosek

      pip install cvxopt
      pip install -f https://download.mosek.com/stable/wheel/index.html Mosek --user
      mkdir ~/mosek
      cp mosek.lic ~/mosek/mosek.lic

* Bazel

      sudo apt-get install openjdk-8-jdk
      echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
      curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
      sudo apt-get update && sudo apt-get install bazel
    
* Download Flower Dataset

      cd inception
      FLOWERS_DATA_DIR=/tmp/flowers-data/
      bazel build //inception:download_and_preprocess_flowers
      bazel-bin/inception/download_and_preprocess_flowers "${FLOWERS_DATA_DIR}"
      
* Copy Algorithms and Necessary Files

      cp ../Simulator/* inception/
      cp ../inception_train.py inception/inception_train.py
      cp ../timeline.json ./timeline.json
      cp ../timeline_memory.json ./timeline_memory.json

## Train
Replace the \<hosts\> with the ip:port of each device seperated by comma. And replace the \<index\> with the task index of the device where 0 indicates the cheif device.

Reference: https://www.tensorflow.org/deploy/distributed
    
    bazel build //inception:flowers_train
    bazel-bin/inception/flowers_train --batch_size=32 --train_dir=/tmp/flowers_train --data_dir=/tmp/flowers-data --hosts=<hosts> --task_index=<index>


